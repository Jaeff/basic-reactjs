import { ITodoFormProp } from "../type";
import './../style.css'
import Box from '@mui/material/Box';
import React from "react";
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function TodoForm(props: ITodoFormProp) {
    const { inputHandler, messageHandler, btnAddHandler, temp_input, temp_message } = props;
    
  return (
    <div>
          
      
      <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '25ch' },
          }}
          noValidate
          autoComplete="off"
      >
      <TextField
          id="outlined-controlled"
          name="title"
          label="Title"
          value={temp_input}
          onChange={inputHandler}
          sx={{
            width: '300px',
            margin: '0 auto',
            backgroundColor: 'transparent',
            color: 'black',
            border: '1px solid #86b7fe',
            borderRadius: '4px',
                          '&:focus': {
            borderColor:  '#86b7fe',
            boxShadow:    '0 0 0 0.1rem rgba(13,110,253,.25)',
        },
      }}
    /><br/>
    <TextField
          name="message"
          label="Message"
          value={temp_message}
          onChange={messageHandler}
          sx={{
            width: '300px',
            margin: '0 auto',
            backgroundColor: 'transparent',
            color: 'black',
            border: '1px solid #86b7fe',
            borderRadius: '4px',
                          '&:focus': {
            borderColor:  '#86b7fe',
            boxShadow:    '0 0 0 0.1rem rgba(13,110,253,.25)',
        },
      }}
    />
    <Stack direction="row"
      spacing={2}
      sx={{
        justifyContent: 'center', // Center horizontally
        alignItems: 'center', // Center vertically
      }}
      >
      <Button onClick={btnAddHandler} variant="contained">
        Add
      </Button>
    </Stack>
    </Box>
    </div>
  );
}

export default TodoForm;

import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { IProp, IState } from './type'
import './style.css'
import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';
import Input from '@mui/joy/Input';
import { Button } from '@mui/material';

function User(props: IProp) {
        // const [isShowForm, setisShowForm] = useState<boolean>(false);
        const [userList, setUserList] = useState<IState['userList']>([]);
        const [showPass, setShowPass] = useState<boolean>(false);
        
        const [state, setState] = useState({
        name: "",
        email: "",
        password: "",
        role: "",
        nameError: "",
        roleError: "",
        emailError: "",
        passwordError: ""
        });
        
        useEffect(() => {
            handleRequestUsers();
        },[]);

        const handleRequestUsers = async () => {
            try {
                const response = await axios.get(
                    "http://localhost:5173/api/user"
                );
                setUserList(response.data);
            } catch (error) {
                alert('ERROR');
            }   
        };

        const nameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
            const { value } = event.target;
            setState({...state, name: value});
        };
    

    
        const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
            const { value } = event.target;
            setState({...state, email: value});
        };
        
        const roleHandler = (event: React.ChangeEvent<HTMLSelectElement>) => {
            const { value } = event.target;
            setState({...state, role: value});
        };

        const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
            const { value } = event.target;
            setState({...state, password: value});
        };

        const validate = () => {
            let nameError = "";
            let passwordError = "";
            let roleError = "";
            let emailError = "";
        
            if (!state.name.trim()) {
              nameError = "Name cannot be blank";
            }
        
            if (!state.password) {
              passwordError = "Password cannot be blank";
            } else if (state.password.length < 8) {
              passwordError = "Password must be at least 8 characters";
            }
        
            if (!state.role) {
              roleError = "Role must be selected";
            }
        
            if (!state.email) {
              emailError = "Email Address cannot be blank";
            }
        
            setState({
              ...state,
              nameError,
              passwordError,
              roleError,
              emailError
            });
            
            return !(nameError || passwordError || roleError || emailError);
          };

          const handleSubmit = async (e) => {
            e.preventDefault(); 
            const isValid = validate();
           
            if (isValid) {
              const id = Date.now().toString();
              const userData = { ...state, id }; 
            try {
              const response = await axios
                .post("http://localhost:5173/api/user", userData)
              
                  console.log(response);
                  alert(`Successfully submitted.`);
        
            }  
            catch (err) {
              console.log('Error:', err);
              alert("Failed to update user. Please try again.");
            }
            setState({
                
                name: "",
                role: "",
                email: "",
                password: "",
                nameError: "",
                roleError: "",
                emailError: "",
                passwordError: ""
            });
            }
          };
        

       
        

    return (
    <div className='center'> 
      <main>
        <CssBaseline />
          <Sheet
            sx={{
              backgroundColor: 'rgba(173, 216, 230, 0.5)',
              backdropFilter: 'blur(10px)',
              width: 400,
              mx: 'auto', // margin left & right
              my: 4, // margin top & bottom
              py: 3, // padding top & bottom
              px: 2, // padding left & right
              display: 'flex',
              flexDirection: 'column',
              gap: 2,
              borderRadius: 'sm',
              boxShadow: 'md',
            }}
            variant="outlined"
          >
            <h1 className='center'>User</h1>
        <br/>
        <br/>
        <form onSubmit={handleSubmit}>
        <div>
        
          <Input
            name="name"
            placeholder="Name"
            value={state.name}
            onChange={nameHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.nameError}</div>
          <br/>
          <br/>
        </div>

        

        <div>
          <Input
            name="email"
            
            placeholder="Email Address"
            value={state.email}
            onChange={emailHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.emailError}</div>
          <br/>
          <br/>
        </div>

        <div>
          
          <Input
            name="password"
            type={showPass ? "text" : "password"} 
            placeholder="Password"
            value={state.password} 
            
            onChange={passwordHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.passwordError}</div>
        
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <input 
              type="checkbox" 
              checked={showPass} 
              onChange={() => setShowPass(prev => !prev)}
          />
          <span style={{ marginRight: '8px' }}> See Passwords</span>
          <br/>
          <br/>
        </div>
        <br/>
          
        <div style={{ textAlign: 'center' }}>
          <select
            name="role"
            value={state.role}
            onChange={roleHandler}
            className="select"
          >
            <option value="">Select Role</option>
            <option value="ADMIN">ADMIN</option>
            <option value="TEAMLEAD">TEAMLEAD</option>
            <option value="INTERN">INTERN</option>
          </select>
          <div>{state.roleError}</div>
          <br/>
          <br/>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <Button type="submit" style={{ marginRight: '8px', backgroundColor: 'blue', color: 'white' }}>Add</Button> 
        </div>
      </form>

          </Sheet>
      </main>      
        
                
            <h1>Details</h1>
            <table className="table-wrapper">
                
                <tbody>
                    <tr>
                        <th className="table-header" style={{ width: '300px' }}>ID</th>
                        <th className="table-header">Name</th>
                        <th className="table-header">Email</th>
                        <th className="table-header">Password</th>
                        <th className="table-header">Role</th>
                        
                    </tr>
                    
                    {userList.map((post) => {
                        return (
                            <tr key={post._id} style={{ textAlign: 'left' }}>
                                <td>{post._id}</td>
                                <td>{post.name}</td>
                                <td>{post.email}</td>
                                <td>{post.password}</td>
                                <td>{post.role}</td>
                            </tr>
                        )
                    })}
                </tbody>
                
            </table>
            
        </div>
              
    )
}

export default User;

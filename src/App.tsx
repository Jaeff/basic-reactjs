
import './App.css'
import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom';

import { protectedRoute, isLoggedIn } from './pages/utils/protectedRoute';
import Home from './pages/Home';
import Login from './pages/Login/index';
import Register from './pages/Register/index';
import Users from './pages/Users/index';
import Post from './pages/Post/index';
import NavBar from './Navbar/index'
import AboutUs from "./pages/About Us/AboutUs"

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    loader: protectedRoute
  },
  
  {
    path: "/Login",
    element: <Login />,
    loader: isLoggedIn
  },
  {
    path: "/Users",
    element: <Users />,
    loader: protectedRoute
  },
  {
    path: "/Navbar",
    element: <NavBar />,
    loader: protectedRoute
  },
  {
    path: "/Post",
    element: <Post />,
    loader: protectedRoute
  },
  {
    path: "/Register",
    element: <Register />,
    loader: isLoggedIn
  },
  {
    path: "/AboutUs",
    element: <AboutUs />,
    loader: protectedRoute
  },
  {
    path: "*",
    element: <h1>404 not found</h1>,
  },
  
])



function App() {
  return (
    <>
          <NavBar/>
          <RouterProvider router={router} />

    </>
  );
}

export default App

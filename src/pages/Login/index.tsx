import React, { useState } from "react";


import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';
import Typography from '@mui/joy/Typography';
import Button from '@mui/joy/Button';
import Link from '@mui/joy/Link';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { IState } from "./type";
import {accounts} from "./../utils/authenAccounts"


export default function Login(props: IState) {
   
   const handleClickShowPassword = () => setShowPassword((show) => !show);
    const [showPassword, setShowPassword] = React.useState(false);
      
  const [state, setState] = useState<IState>({
    email: '',
    password: '',
    showSetting: false
    })

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };
    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setState({...state, email: value});
    };
  
  const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;
      setState({...state, password: value});
    };

      
    const handleShowSetting = () => {
      setState({ ...state, showSetting: true });
    };

      

          
    const HandlerLogin = () => {
      console.log(state.email)
      console.log(state.password)
      const token = {
        email: state.email,
        password: state.password,
      };
      console.log(token)
      const authenticated = accounts.map((item: any) => JSON.stringify(item)).includes(JSON.stringify(token));
      if (authenticated) {
        
          localStorage.setItem("Token", JSON.stringify(token));
          handleShowSetting()
          window.location.href = './';
          
      } else {
          alert("Invalid Account");
      }
      
    };
      

  return (
    <div>
    <main>
      <CssBaseline />
      <Sheet
        sx={{
          backgroundColor: 'rgba(173, 216, 230, 0.5)',
          backdropFilter: 'blur(10px)',
          width: 300,
          mx: 'auto', // margin left & right
          my: 4, // margin top & bottom
          py: 3, // padding top & bottom
          px: 2, // padding left & right
          display: 'flex',
          flexDirection: 'column',
          gap: 2,
          borderRadius: 'sm',
          boxShadow: 'md',
        }}
        variant="outlined"
      >
        <div>
          <Typography level="h4" component="h1">
            <b>Welcome!</b>
          </Typography>
          <Typography level="body-sm">Sign in to continue.</Typography>
        </div>
        <FormControl>
        <InputLabel htmlFor="outlined-adornment-password">Email</InputLabel>
          <OutlinedInput
          style = {{height: 47}}
            value={state.email}
            onChange={emailHandler}
            type="email"
            label="Email"
          />
        </FormControl>
        <FormControl >
          <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
          <OutlinedInput
          style = {{height: 47}}
            id="outlined-adornment-password"
            type={showPassword ? 'text' : 'password'}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
            onChange={passwordHandler}
            label="Password"
            value={state.password}
          />
        </FormControl>
        <Button onClick={HandlerLogin} sx={{ mt: 1 /* margin top */ }}>Log in</Button>
        <Typography
          endDecorator={<Link href="/Register">Sign up</Link>}
          fontSize="sm"
          sx={{ alignSelf: 'center' }}
        >
          Don&apos;t have an account?
        </Typography>
      </Sheet>
    </main>
  </div>                  
  );
}
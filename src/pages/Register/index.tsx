import React, { useState } from "react";
import axios from "axios";
import Input from '@mui/joy/Input';
import './style.css'
import Sheet from '@mui/joy/Sheet';
import CssBaseline from '@mui/joy/CssBaseline';
import Typography from '@mui/joy/Typography';

const Register = () => {
  const [showPass, setShowPass] = useState<boolean>(false);
  const [state, setState] = useState({
    name: "",
    password: "",
    email: "",
    role: "",
    nameError: "",
    passwordError: "",
    roleError: "",
    emailError: ""
  });

  const nameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
      setState({...state, name: value});
  };

  const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
      setState({...state, password: value});
  };

  const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
      setState({...state, email: value});
  };

  const roleHandler = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const { value } = event.target;
      setState({...state, role: value});
  };

  const validate = () => {
    let nameError = "";
    let passwordError = "";
    let roleError = "";
    let emailError = "";

    if (!state.name.trim()) {
      nameError = "Name cannot be blank";
    }

    if (!state.password) {
      passwordError = "Password cannot be blank";
    } else if (state.password.length < 8) {
      passwordError = "Password must be at least 8 characters";
    }

    if (!state.role) {
      roleError = "Role must be selected";
    }

    if (!state.email) {
      emailError = "Email Address cannot be blank";
    }

    setState({
      ...state,
      nameError,
      passwordError,
      roleError,
      emailError
    });

    return !(nameError || passwordError || roleError || emailError);
  };

  const handleSubmit = async (e) => {
    e.preventDefault(); 
    const isValid = validate();
   
    if (isValid) {
      const id = Date.now().toString();
      const userData = { ...state, id }; 
    try {
      const response = await axios
        .post("http://localhost:5173/api/user", userData)
      
          console.log(response);
          alert(`Successfully submitted.`);

    }  
    catch (err) {
      console.log('Error:', err);
      alert("Failed to update user. Please try again.");
    }

      setState({
        name: "",
        password: "",
        role: "",
        email: "",
        nameError: "",
        passwordError: "",
        roleError: "",
        emailError: ""
      });
    }
  };

  return (
    <main>
      <CssBaseline />
      <Sheet
        sx={{
          backgroundColor: 'rgba(173, 216, 230, 0.5)',
          backdropFilter: 'blur(10px)',
          width: 400,
          mx: 'auto', // margin left & right
          my: 4, // margin top & bottom
          py: 3, // padding top & bottom
          px: 2, // padding left & right
          display: 'flex',
          flexDirection: 'column',
          gap: 2,
          borderRadius: 'sm',
          boxShadow: 'md',
        }}
        variant="outlined"
      >
      <div>
          <Typography level="h4" component="h1">
            <b>Welcome!</b>
          </Typography>
          <Typography level="body-sm">Sign Up to continue.</Typography>
        </div>  
        <form onSubmit={handleSubmit}>
        <div>
        
          <Input
            name="name"
            placeholder="Name"
            value={state.name}
            onChange={nameHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.nameError}</div>
          <br/>
          <br/>
        </div>

        

        <div>
          <Input
            name="email"
            
            placeholder="Email Address"
            value={state.email}
            onChange={emailHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.emailError}</div>
          <br/>
          <br/>
        </div>

        <div>
          
          <Input
            name="password"
            type={showPass ? "text" : "password"} 
            placeholder="Password"
            value={state.password} 
            
            onChange={passwordHandler}
            sx={{
              width: '300px',
              margin: '0 auto',
              backgroundColor: 'transparent',
              border: '1px solid black',
              borderRadius: '4px',
              '&:focus': {
              borderColor: '#86b7fe',
              boxShadow: '0 0 0 0.1rem rgba(13,110,253,.25)',
              },
            }}
          />
          <div>{state.passwordError}</div>

        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <input 
              type="checkbox" 
              checked={showPass} 
              onChange={() => setShowPass(prev => !prev)}
          />
          <span style={{ marginRight: '1px' }}> See Passwords</span>
          <br/>
          <br/>
        </div>
        <div style={{ textAlign: 'center' }}>
          <br/>
          <select
            name="role"
            value={state.role}
            onChange={roleHandler}
            className="select"
          >
            <option value="">Select Role</option>
            <option value="ADMIN">ADMIN</option>
            <option value="TEAMLEAD">TEAMLEAD</option>
            <option value="INTERN">INTERN</option>
          </select>
          <div>{state.roleError}</div>
          <br/>
          <br/>
        </div>
         <div style={{ textAlign: 'center' }}>   
            <button type="submit" className="btn-Login">Submit</button>
            <br/><br/>
            <p>Already have a account? <a href="/Login">Click here!</a></p>
        </div>
      </form>
      </Sheet>    
    </main>
  );
  
};

export default Register;

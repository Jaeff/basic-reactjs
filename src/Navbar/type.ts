export interface ITodoDisplayProp {
    
    showSetting: boolean;
    actionLogOut: (value: any) => void;
}

export interface IProp {}

export interface IState {
    email: string;
    password: string;
    showSetting: boolean;
    
}


export interface ITodoDisplay {
    
    email: string;
    password: string;
    showSetting: boolean;
    actionTodoLogOut: (value: any) => void;
    passwordHandler: (value: any) => void;
    emailHandler: (value: any) => void;
    actionLogIn: (value: any) => void;
    actionEmail: (value: any) => void;
    actionPassword: (value: any) => void;
}
export interface IProp {}

export interface IState {
    postList: IPost[];
    
}

interface IPost {
    id: string;
    _id: string;
    userId: string;
    title: string;
    message: string;
    status: 'ACTIVE' | 'INACTIVE';
}

export interface IStateNewForm {
    id: string;
    title: string;
    message: string;
}

interface ITodo {
    _id: string;
    input: string;
    message: string;
}

// export interface IPost {
//     // id: string;
//     _id: string;
//     userId: string;
//     title: string;
//     message: string;

// }


// interface ITodo {
//     id: string;
//     input: string;
//     message: string;
// }

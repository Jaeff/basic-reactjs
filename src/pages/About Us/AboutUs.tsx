import React from "react";


const Register = () => {
  


  return (
    <>
    <br/>
    <h2 style={{ textAlign: "center"}}>
    Who We Are</h2>
    <br/>
    <br/>
    <p style={{ textAlign: "center"}}>
    JobTarget is a company dedicated to helping job seekers and employers connect. We are a team of nearly 500 who are passionate about improving online job search. This focus has allowed us to earn the opportunity to serve thousands of companies and millions of job seekers each month. We are proud and grateful for the opportunity to help them.

    <br/>
    <br/>
    We’ve never been one of those high-flying startups, raising gazillions of dollars, and plastering our ads on TV and radio. Instead we’ve focused on developing the best products, growing slowly, and maintaining the highest level of service we can for our customers and users. We understand that every day we need to earn their trust – and we take that to heart.
    <br/>
    <br/>
    
    Thank you for visiting us and learning a little bit about us. Hopefully, you too will find some value in JobTarget’s services and will give us the opportunity to serve you as well.
    </p>
    </>
  );
  
};

export default Register;

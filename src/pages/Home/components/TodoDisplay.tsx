import "../style.css";
import { ITodoDisplayProp } from "../type";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import Button from '@mui/material/Button';
 
function TodoDisplay(props: ITodoDisplayProp) {
  const { inputlist, showTable,  editItem, actionDelete, actionEdit, handeCancelButton, handleEditInput, handeUpdateButton } = props;
  
  return (
    <>
    
      {!showTable ? (
        <></>
      ) : (
        <table style={{ borderRadius: '10px'}} className="table-wrapper">
          <tbody>
            <tr>
              <th style={{ backgroundColor: "#ccc"}}>ID</th>
              <th style={{ backgroundColor: "#ccc" }}>Title</th>
              <th style={{ backgroundColor: "#ccc" }}>Message</th>
              <th style={{ backgroundColor: "#ccc" }}>Action</th>
            </tr>
            {inputlist.map((items, index) => {
              if (items.id === editItem?.id && items.id) {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.id}</span>
                    </td>
                    <td>
                      <input name="editInput" type="text" value={editItem.input} onChange={handleEditInput}/>
                    </td>
                    <td>
                      <input name="editMessage" type="text" value={editItem.message} onChange={handleEditInput}/>
                    </td>
                    <td>
                      <button className="btn-btn-update" onClick={handeUpdateButton}>Update</button>
                      <button className="btn-btn-cancel" onClick={handeCancelButton}>Cancel</button>
                    </td>
                  </tr>
                );
              } else {
                return (
                  <tr key={items.id + index}>
                    <td>
                      <span>{items.id}</span>
                    </td>
                    <td>
                      <span>{items.input}</span>
                    </td>
                    <td>{items.message}</td>
                    <td>
                      <Button
                        style={{ backgroundColor: 'transparent', color: 'inherit' }}
                        onClick={() => actionEdit(items)}
                        startIcon={<EditIcon />}
                      />
                      <Button
                        style={{ backgroundColor: 'transparent', color: 'red' }}
                        onClick={() => actionDelete(items.id)}
                        startIcon={<DeleteIcon />}
                      />
                       
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
      )}
    </>
  );
}

export default TodoDisplay;

import React from "react";
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import  './style.css';
import Box from '@mui/material/Box';

import MenuItem from '@mui/material/MenuItem';

import Nav from 'react-bootstrap/Nav';


function ResponsiveAppBar() {
  // const [ isSetting, setIsSetting ] = React.useState<boolean>(true)
  const token = localStorage.getItem('Token');
  
  
  const handleLogout = () => {
    let confirmSignOut = "Are you sure you want to SignOut?";
    if (confirm(confirmSignOut)) {
      localStorage.removeItem('Token');
      
        alert('Thank you for visiting, Come Back again!')
      window.location.href = './Login';
      
      
    }
    
  };
  
  
  
  return (
    <div>
  <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static" sx={{ backgroundColor: 'transparent', touchAction: 'none', backgroundAttachment: 'fixed' }}>
      <Container maxWidth="xl" >
        <Toolbar variant="dense">
          
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 2,
              display: { xs: 'none', md: 'flex' },
              fontFamily: 'monospace',
              fontWeight: 700,
              letterSpacing: '.3rem',
              color: 'inherit',
              textDecoration: 'none',
              justifyContent: 'start'
            }}
          >
            <img src='src/Navbar/img/jobtarget-logo.png' alt='logo' style={{ width: '100px', height: 'auto', color:"white" }} />
            
          </Typography>
            <Nav  style={{ justifyContent: "space-between" }}>
                {token ? 
                (
                  <>
                    <MenuItem>
                      <Nav.Link href="/" style={{ marginRight: '8px' }}><strong>Home</strong></Nav.Link> 
                      <Nav.Link href="/Post" style={{ marginRight: '8px' }}><strong>Post</strong></Nav.Link>
                      <Nav.Link href="/Users" style={{ marginRight: '8px' }}><strong>User</strong></Nav.Link>
                      <Nav.Link href="/AboutUs" style={{ marginRight: '8px' }}><strong>About Us</strong></Nav.Link>
                    </MenuItem>
                  </>
                ) 
                  : 
                    "Hello Gwapo/Gwapa!!"
                }
            </Nav>
            <Nav style={{ marginLeft: 'auto', display: 'flex', justifyContent: 'flex-end' }}>
                  {token ? 
                    <Nav.Link onClick={handleLogout} ><strong>Logout</strong></Nav.Link> 
                  : 
                    <Nav.Link href="/Login"></Nav.Link>
                  }
            </Nav>      

           
          
          
        </Toolbar>
      </Container>
    </AppBar> 

</Box>
  </div>
  );
  
}

export default ResponsiveAppBar;



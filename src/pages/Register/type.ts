export interface IProp {}

export interface IState {
    
    id: number;
    name: string;
    username: string;
    password: string;
    email: string
    role: 'ADMIN' | 'TEAMLEAD' | 'INTERN'
    
}